![](https://miro.medium.com/proxy/1*KIvhyNif1TCFJxt6kz-gsA.jpeg)

> Before you run the application on device or emulator, make sure you have the environment complete set up for this
> project.
>
> - [React Native Official Docs](https://reactnative.dev/docs/environment-setup).


### First Steps

make sure you read all the basics steps for run an aplication with **React Native Cli** or **Expo**. You can use **npm** or **yanr** as your package manager.

How do you know if you project has **npm** or **yarn**, Just go the root folder and if you see a file **yarn.lock** the project use **Yarn** on the other hand if you see **package-lock.json** the project use **npm**

- To install **yarn** please follow [Yarn Official Docs](https://yarnpkg.com/getting-started)

- To install **npm** please follow [Npm CLI](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

- Make sure you have **git** install on your machine, run ```git --version``` if you not see the version go to [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

- Clone your code via **git**, **strongly recommend using SSH** mode, if you don't have set up your git account to use ssh security please.
Add an SSH key for secure access to GitLab [Learn more](https://docs.gitlab.com/ee/user/ssh.html)

- For Android please make sure you have access to execute **gradlew** file, usually when you are in unix os environment
(Mac/Linux). Run the below commands in root project
    ```bash
    cd android && chmod +x gradlew
    ```


### Extras & Notes
- To build an .apk or .aab for **Android**.
  - [Publishing to Google Play Store](https://reactnative.dev/docs/signed-apk-android)

- To build an .ipa or upload to **App Store**.
  - [Publishing to Apple App Store](https://reactnative.dev/docs/publishing-to-app-store)

- Recommend to use **Hermes** improved start-up time, decreased memory usage, and smaller app size.
  - [Using Hermes](https://reactnative.dev/docs/hermes)
- To set up a global git user or just for the branch you're working. 
  - [Setting your username in Git](https://docs.github.com/en/get-started/getting-started-with-git/setting-your-username-in-git)

- A security tips if you want to add some layer of security [blog](https://dev.to/acronimax/add-security-when-build-mobile-apps-18h1)

#### Useful Commands

Some commands that i found useful when strugul with some bug.

>  React Native Troubleshooting.
> 
> - Stop cached listeners ```watchman watch-del-all```
> - Clear watchman instances ```watchman shutdown-server```
> - Remove installed modules ```rm -rf node_modules```
> - Remove yarn meta files ```rm yarn*```
> - Install only fresh copies ```yarn cache clean```
> - Kill any other instance of the packager ```lsof -ti:8081 | xargs kill```
> - Restart the thing ```npm start --reset-cache```
> 
> **Use as you need!**
